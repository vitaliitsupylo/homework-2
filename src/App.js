import React from 'react';
import LoaderImg from "./components/Image";
import Table from "./components/Table";
import Row from "./components/Row";
import Cell from './components/Cell';
import './App.css';

function App() {
    return (
        <div className="App">
            <LoaderImg className="image"
                   src={'https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png'}/>
            <Table>
                <Row head="true">
                    <Cell type="" background="red">#</Cell>
                    <Cell type="date">2</Cell>
                    <Cell type="number">3</Cell>
                    <Cell type="money" currency="$">4</Cell>
                </Row>
                <Row>
                    <Cell type="" background="red">1</Cell>
                    <Cell type="date">2</Cell>
                    <Cell type="number">3</Cell>
                    <Cell type="money" currency="$">4</Cell>
                </Row>
            </Table>
        </div>
    );
}

export default App;
