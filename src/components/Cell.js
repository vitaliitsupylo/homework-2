import React from "react";

function Cell({cells, background, color, type, currency, children}) {
    const style = {
        background,
        color,
        border: 'solid 1px #ccc'
    };

    switch (type) {
        case 'date':
            style.fontStyle = 'italic';
            break;
        case 'number':
            style.textAlign = 'right';
            break;
        case 'money':
            style.textAlign = 'right';
            break;
        default:
            style.textAlign = 'left';
            break;
    }

    return (<td colSpan={cells} style={style}>{children}{(currency) ? currency : null}</td>)
}

Cell.defaultProps = {
    type: 'text',
    cells: 1,
    background: 'transparent',
    color: 'black',
    border: 'solid 1px #ccc'
};

export default Cell;