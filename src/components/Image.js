import React, {Component, Fragment} from "react";

export default class Img extends Component {

    state = {
        loading: true,
        loadError: false
    };

    componentDidMount() {
        console.log(this.state.src);
    }

    loadImage = () => {
        this.setState({loading: false});
    };

    errorLoadImage = () => {
        this.setState({loadError: true, loading: false});
    };

    render() {
        const {loading, loadError} = this.state;
        const {src = '', alt = '', className = ''} = this.props;
        const {loadImage, errorLoadImage} = this;

        return (
            <>
                <img className={(loading) ? `none ${className}` : className} src={src} alt={alt} onLoad={loadImage}
                     onError={errorLoadImage}/>
                {(loading) ? <span className='loading'>Loading...</span> : null}
                {(loadError) ? <span className='loading-error'>Loading error!</span> : null}
            </>
        );
    }
}