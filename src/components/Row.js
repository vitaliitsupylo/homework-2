import React from "react";

function Row({head, className, children}) {
    return (
        <tr style={(head === 'true') ? {textTransform: 'uppercase'} : null}>{children}</tr>
    );
}

Row.defaultProps = {
    head: 'false'
};


export default Row;