import React from "react";

export default function Table({className, children}) {
    return (
        <table className={(className) ? className : 'table'}>{children}</table>
    )
}